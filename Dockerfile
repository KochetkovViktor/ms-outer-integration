FROM nexus.trosbank.trus.tsocgen/nicb/alpine-openjdk11-jre:latest
RUN curl -k -o /usr/bin/grpc-health-probe https://nexus.trosbank.trus.tsocgen/repository/nicb-thirdparty/grpc/grpc-health-probe/v0.3.0/grpc-health-probe-v0.3.0-linux-amd64.bin && \
    chmod 777 /usr/bin/grpc-health-probe

WORKDIR /app
COPY /build/libs/outer-integration-*.jar outer-integration.jar

ENTRYPOINT ["java", "-jar", "outer-integration.jar"]
