package ru.rb.integration.config;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

@Configuration
@ConditionalOnProperty(name = "application.swaggerApi", havingValue = "true")
@EnableSwagger2
public class SwaggerConfig {

    @Autowired
    private TypeResolver typeResolver;

    @Bean
    public UiConfiguration uiConfig() {
        return new UiConfiguration(
            "/swagger",
            "none",
            "alpha",
            "schema",
            UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS,
            false,
            true);
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .paths(PathSelectors.ant("/api/outer-integration/**"))
            .build()

            .directModelSubstitute(Resource.class, Byte[].class)
            .directModelSubstitute(MultipartFile.class, Byte[].class)
            .ignoredParameterTypes(MultipartFile.class)
            .ignoredParameterTypes(Resource.class)
            .ignoredParameterTypes(File.class)
            .ignoredParameterTypes(URI.class)
            .ignoredParameterTypes(URL.class)
            .ignoredParameterTypes(InputStream.class)
            .additionalModels(typeResolver.resolve(Error.class))
            .globalResponseMessage(RequestMethod.GET, getGlobalResponse())
            .globalResponseMessage(RequestMethod.POST, getGlobalResponse())
            .globalResponseMessage(RequestMethod.PUT, getGlobalResponse())
            .globalResponseMessage(RequestMethod.DELETE, getGlobalResponse())
            ;
    }

    public List<ResponseMessage> getGlobalResponse() {
        return Arrays.asList(
            new ResponseMessageBuilder()
                .code(200)
                .message("OK")
                .build(),
            new ResponseMessageBuilder()
                .code(400)
                .message("400 message")
                .responseModel(new ModelRef("Error"))
                .build(),
            new ResponseMessageBuilder()
                .code(500)
                .message("Internal Error")
                .build());
    }
}
