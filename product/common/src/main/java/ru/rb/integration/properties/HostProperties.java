package ru.rb.integration.properties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HostProperties {

    private String host;

    private Integer port;

    private String balancingPolicy;
}
