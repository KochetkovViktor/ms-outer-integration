package ru.rb.integration.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@ConfigurationProperties("pagination")
public class PaginationProperties {

    private Long itemLimit;

    private Long maxItemLimit;
}
