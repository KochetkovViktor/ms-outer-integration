package ru.rb.integration.grpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rb.api.grpc.deposits.v1.DepositAdminApiGrpc;
import ru.rb.api.grpc.deposits.v1.ImportRatesFile;
import ru.rb.api.grpc.deposits.v1.ImportRatesFileResponse;
import ru.rb.integration.properties.GrpcProperties;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class DepositGrpcClient {

    @Autowired
    private GrpcProperties config;

    private DepositAdminApiGrpc.DepositAdminApiBlockingStub depositService;

    @PostConstruct
    private void init() {
        ManagedChannel managedChannel = ManagedChannelBuilder
                .forAddress(
                        config.getDeposits().getHost(),
                        config.getDeposits().getPort()
                ).usePlaintext().build();

        depositService = DepositAdminApiGrpc.newBlockingStub(managedChannel);//Отключаем до уточнения использования аудита .withInterceptors(GrpcUtils.getInterceptor());
    }

    public ImportRatesFileResponse importRatesXlsm(ImportRatesFile request) {
        return depositService.importRatesXlsm(request);
    }
}