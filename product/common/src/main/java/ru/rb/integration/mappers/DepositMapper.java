package ru.rb.integration.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import ru.rb.api.grpc.deposits.v1.ImportRatesFile;
import ru.rb.api.grpc.util.GoogleProtobufUtil;
import ru.rb.api.grpc.util.WrapperUtil;
import ru.rb.rest.model.deposit.ImportRatesResponse;

@Mapper(uses = { WrapperUtil.class, GoogleProtobufUtil.class },
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DepositMapper {

    ImportRatesFile toGrpc(ru.rb.rest.model.deposit.ImportRatesRequest source);

    ImportRatesResponse toDomain(ru.rb.api.grpc.deposits.v1.ImportRatesFileResponse source);
}
