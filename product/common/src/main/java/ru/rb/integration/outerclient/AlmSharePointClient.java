package ru.rb.integration.outerclient;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import ru.rb.rest.model.deposit.AlmProperties;

@Slf4j
@Component
public class AlmSharePointClient {

    private AlmProperties almProperties;
    private static final int DEFAULT_CONNECTION_TIMEOUT_IN_SECOND = 120;
    private static final int PORT = 443;
    private static final String URL_SCHEMA = "https";
    private static final HttpClientContext CLIENT_CONTEXT = HttpClientContext.create();
    private static final CredentialsProvider CREDENTIALS_PROVIDER = new BasicCredentialsProvider();
    private static final SimpleClientHttpRequestFactory HTTP_REQUEST_FACTORY = new SimpleClientHttpRequestFactory();
    private static final CloseableHttpClient HTTP_CLIENT = HttpClients.custom()
        .setRetryHandler(new DefaultHttpRequestRetryHandler(0, false))
        .build();

    public AlmSharePointClient(AlmProperties almProperties) {
        this.almProperties = almProperties;

        HTTP_REQUEST_FACTORY.setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT_IN_SECOND);
        HTTP_REQUEST_FACTORY.setReadTimeout(DEFAULT_CONNECTION_TIMEOUT_IN_SECOND);

        CREDENTIALS_PROVIDER.setCredentials(AuthScope.ANY,
            new NTCredentials(almProperties.getSharePointUserName(), almProperties.getSharePointUserPassword(),
                "", almProperties.getSharePointDomain()));
        CLIENT_CONTEXT.setCredentialsProvider(CREDENTIALS_PROVIDER);
    }

    /**
     * Получение файла со ставками с сервера Share Point, преобразование в массив байт
     * @param fileName путь к файлу
     * @return массив байт эксель-файла
     */
    public byte[] getFile(String fileName) throws Exception {
        //String requestUrl = almProperties.getAlmSharePointUrl();
        String requestUrl = fileName.substring(0,fileName.lastIndexOf("/"));
        fileName = fileName.substring(fileName.lastIndexOf("/"), fileName.length()-1);
        log.debug("requestUrl: " + requestUrl);
        HttpHost target = new HttpHost(requestUrl, PORT, URL_SCHEMA);

        initialRequest(target);
        return getHttpEntity(target, fileName);
    }

    private void initialRequest(HttpHost target) throws Exception {
        HttpHead authRequest = new HttpHead("/");
        try (CloseableHttpResponse response = HTTP_CLIENT.execute(target, authRequest, CLIENT_CONTEXT)) {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    private byte[] getHttpEntity(HttpHost target, String fileName) throws Exception {
        HttpGet downloadRequest = new HttpGet("/_api/web/GetFileByServerRelativeUrl('" + fileName + "')/$value");
        try (CloseableHttpResponse response = HTTP_CLIENT.execute(target, downloadRequest, CLIENT_CONTEXT)) {
            HttpEntity entity = response.getEntity();

            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            String reason = statusLine.getReasonPhrase();

            if (HttpStatus.SC_OK == statusCode) {
                return EntityUtils.toByteArray(entity);
            } else {
                throw new Exception(String.format("Problem while receiving %s reason : %s httpcode : %s",
                    fileName, reason, statusCode));

            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }
}
