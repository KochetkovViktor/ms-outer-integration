package ru.rb.health.check.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rb.health.check.model.Healthcheck;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

@RestController
@RequestMapping()
@Validated
@Api(value = "healthcheck", description = "the healthcheck API", hidden = true, tags = "Health")
public class HealthController {

    @ApiOperation(value = "Health Check", notes = "The healthcheck endpoint provides detailed information about the health of a web service. If each of the components required by the service are healthy, then the service is considered healthy and will return a 200 OK response. If any of the components needed by the service are unhealthy, then a 503 Service Unavailable response will be provided.",
            nickname = "healthcheckGet", hidden = true)
    @GetMapping(value = "/healthcheck", produces = {"application/json"})
    public ResponseEntity<Healthcheck> healthcheckGet() {
        OffsetDateTime start = OffsetDateTime.now();
        return ResponseEntity.ok(new Healthcheck() {{
            setGeneratedAt(start);
            setDurationMillis(new BigDecimal(start.toInstant().until(Instant.now(), ChronoUnit.MILLIS)));
        }});
    }
}
