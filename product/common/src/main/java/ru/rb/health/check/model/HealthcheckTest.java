package ru.rb.health.check.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@ApiModel
public class HealthcheckTest   {

  @ApiModelProperty(required = true, value = "The IS08601 date and time representing the start of the test")
  @NotNull
  @Valid
  @JsonProperty("tested_at")
  private OffsetDateTime testedAt;

  @ApiModelProperty(required = true, value = "The result of running the test")
  @NotNull
  @JsonProperty("result")
  private ResultEnum result;

  @ApiModelProperty(required = true, value = "The number of milliseconds that the test took to executed")
  @NotNull
  @Valid
  @JsonProperty("duration_millis")
  private BigDecimal durationMillis;

  @ApiModelProperty(value = "A string describing any error conditions that were experienced")
  @JsonProperty("error")
  private String error;

}

