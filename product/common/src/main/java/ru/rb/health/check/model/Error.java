package ru.rb.health.check.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@Data
public class Error   {

  @ApiModelProperty(value = "")
  @JsonProperty("code")
  private Integer code;

  @ApiModelProperty(value = "")
  @JsonProperty("message")
  private String message;

  @ApiModelProperty(value = "")
  @JsonProperty("fields")
  private String fields;

}

