package ru.rb.health.check.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

@ApiModel
@Data
public class Healthcheck   {

  @ApiModelProperty(required = true, value = "The ISO8601 date and time representing the start of the healthcheck report")
  @NotNull
  @Valid
  @JsonProperty("generated_at")
  private OffsetDateTime generatedAt;

  @ApiModelProperty(required = true, value = "The number of milliseconds that the healthecheck report took to generate")
  @NotNull
  @Valid
  @JsonProperty("duration_millis")
  private BigDecimal durationMillis;

  @ApiModelProperty(required = true, value = "")
  @NotNull
  @JsonProperty("tests")
  @Valid
  private Map<String, Map<String, HealthcheckTest>> tests = new HashMap<>();

}
