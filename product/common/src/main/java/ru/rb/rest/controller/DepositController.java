package ru.rb.rest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rb.integration.grpc.DepositGrpcClient;
import ru.rb.integration.mappers.DepositMapper;
import ru.rb.integration.outerclient.AlmSharePointClient;
import ru.rb.rest.model.deposit.AlmProperties;
import ru.rb.rest.model.deposit.AlmRequest;
import ru.rb.rest.model.deposit.ImportRatesRequest;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/integration/deposit")
@Validated
@Slf4j
@Api(description = "the Deposit Integration API", tags = "Deposits integration")
public class DepositController {

    private AlmProperties almProperties;
    private DepositGrpcClient client;
    private AlmSharePointClient almClient;
    private static final DepositMapper mapper = Mappers.getMapper(DepositMapper.class);;

    @Autowired
    public DepositController(DepositGrpcClient client, AlmProperties almProperties, AlmSharePointClient almClient) {
        this.client = client;
        this.almProperties = almProperties;
        this.almClient = almClient;
    }

    @ApiOperation(value = "Прием уведомления об изменении файла со ставками",
        notes = "Начинается автоматическая загрузка ставок из SharePoint")
    @PostMapping("/login")
    public ResponseEntity receiveRequestForDownloadAlmRates(@Valid AlmRequest request) {
        try {
            String fileName = request.getFilename();
            log.debug("filename: {}", fileName);
            if (authorization(request)) {
                return importFile(fileName);
            } else {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    private boolean authorization(AlmRequest request) {
        boolean response = false;
        String username = request.getUsername();
        if (almProperties.getUsername().equals(username) && almProperties.getPassword().equals(request.getPassword())) {
            log.debug("alm user {} logged in successfully", username);
            response = true;
        } else {
            log.error("alm user {} not logged in", username);
        }
        return response;
    }

    private ResponseEntity importFile(String name) {
        try {
            log.debug("Getting file process starts...");
            client.importRatesXlsm(mapper.toGrpc(fillRequest(name)));
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private ImportRatesRequest fillRequest(String name) throws Exception {
        ImportRatesRequest request = new ImportRatesRequest();
        byte[] byteArray = almClient.getFile(name);
        request.setContent(byteArray);
        return request;
    }
}
