package ru.rb.rest.model.deposit;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Запрос аутентификации из sharepoint")
@Getter
@Setter
@RequiredArgsConstructor
@Data
public class AlmRequest {
    @ApiModelProperty(value = "Логин")
    String username;
    @ApiModelProperty(value = "Пароль")
    String password;
    @ApiModelProperty(value = "Название файла")
    String filename;
}