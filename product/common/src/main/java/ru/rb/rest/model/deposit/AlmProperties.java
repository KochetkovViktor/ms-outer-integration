package ru.rb.rest.model.deposit;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("security.alm")
@Getter
@Setter
@RequiredArgsConstructor
@Component
public class AlmProperties {

    /** Логин, с которым приходит запрос при изменении файла на sharePoint */
    private String username;
    /** Пароль, с которым приходит запрос при изменении файла на sharePoint */
    private String password;
    /** Логин доменной учетки, с которой ИКБ запрашивает файл с sharePoint */
    private String sharePointUserName;
    /** Пароль доменной учетки, с которой ИКБ запрашивает файл с sharePoint */
    private String sharePointUserPassword;
    /** Адрес сайта sharePoint */
    private String almSharePointUrl;
    /** Домен учетной записи */
    private String sharePointDomain;
}
