package ru.rb.rest.model.deposit;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class ImportRatesRequest {

    /** Файл, полученный с SharePoint, формата xlsm, в виде массива байт */
    byte[] content;
}
