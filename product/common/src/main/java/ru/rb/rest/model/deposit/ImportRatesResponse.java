package ru.rb.rest.model.deposit;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class ImportRatesResponse {

    /** Номер версии загруженного файла со ставками */
    int version;
}
